# APUConfig

#### 介绍
小程序绑定Wi-Fi设备,绑定源码

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明
	APUConfig配网绑定流程
	设备端(请自行实现设备端流程)
	1.获取设备MAC XX:XX:XX:XX:XX:XX
	2.控制WIFI发出固定无线网  名称:wifi_8266_bind    密码:11223344  
	3.UDP 监听固定端口5556
	4.等待接收客户端的消息. 消息格式{"ssid":"qqqqq","pwd":"11223344"} 
	5.提取路由器名称和密码,连接路由器
	6.获取链接路由器后分的的IP. 假设是192.168.10.2 以防后期实现局域网通信备用 
	7.UDP发送数据,{"mac":"XX:XX:XX:XX:XX:XX","ip":"192.168.10.2"}

	APP/小程序/上位机
	1.提示用户连接自己的路由器,长按设备按钮使得设备进入UDP监听状态,提示用户输入路由器密码
	2.用户点击绑定设备 , 控制手机尝试连接 名称为 wifi_8266_bind 的无线 (内部控制)
	3.成功连接无线,往192.168.4.1:5556 UDP发送路由器信息,1S 1次
	4.接收到 {"mac":"XX:XX:XX:XX:XX:XX","ip":"192.168.10.2"}
	5.绑定成功
	8.完
  
1.  把APConfig全部文件导入工程

2.  请在util.js添加如下代码(该代码用于提取UDP数据)

	// util.newAb2Str代码
	var newAb2Str =   function newAb2Str(arrayBuffer) {  
	  let unit8Arr = new Uint8Array(arrayBuffer);  
	  let encodedString = String.fromCharCode.apply(null, unit8Arr),  
	    decodedString = decodeURIComponent(escape((encodedString)));//没有这一步中文会乱码  
	  return decodedString;  
	}  

	module.exports = {  
	  formatTime: formatTime,  
	  newAb2Str: newAb2Str  
	}  

3.  编写跳转到 APUConfig 函数,请在自己需要的地方自行填写
	wx.navigateTo  
	({  
		url: '../APUConfig/APUConfig'   //跳转到绑定页面  
	})  

4.  绑定完成,跳转到的页面  
	在源码 APUConfig.js大约 100 - 114行  
	if (str!=null)  
	{  
		let json = JSON.parse(str);//解析JSON数据  
		if (json != null)  
		{  
			let mac = json.mac;  
			let ip = json.ip;  
			if (mac != null)  
			{   
				wx.reLaunch({  
				url: '../index/index?ClientID=' + mac + "&" + "IP=" + ip  
				})  
			}  
		}  
	}  

5.在跳转的页面获取绑定的数据  
  /**  
   * 生命周期函数--监听页面加载  
   */  
  onLoad: function (options)   
  {  
    let _this = this;  
    console.log(options.ClientID);//获取跳转过来的数据  
  }  

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
