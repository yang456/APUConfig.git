#ifndef __MAIN_H_
#define __MAIN_H_
#include <stm32f10x.h>

#ifndef __MAIN_C_
#define __MAIN_EXT_ extern
#else
#define __MAIN_EXT_
#endif

/*
如果使用自定义:
	发布的主题:1111
	遗嘱发布的主题:同上
	订阅的主题:2222
不使用自定义:	
	发布的主题:devic/WIFI的MAC或者GPRS的IMEI
	遗嘱发布的主题:同上
	订阅的主题:user/WIFI的MAC或者GPRS的IMEI
*/
//#define UserCustomConfig  //使用自定义配置


//缓存数据使用
#define MainBufferLen 200
__MAIN_EXT_ char MainBuffer[MainBufferLen];//缓存数据,全局通用
__MAIN_EXT_ u32  MainLen;      //全局通用变量
__MAIN_EXT_ char *MainString;    //全局通用变量


#endif
