/**
  ******************************************************************************
  * @author  yang feng wu 
  * @version V1.0.0
  * @date    2019/10/12
  * @brief   配置8266
  ******************************************************************************
	一,使用说明:指示灯(PC13)
		1,把以下程序放在1ms定时器中断中
			SmartConfigCnt++;
			if(SmartConfigFlage)//配网状态,指示灯闪耀
			{
				Config8266LedDelay++;
				if(Config8266LedDelay>100)
				{
					Config8266LedDelay=0;
					SmartConfigPinOut = ~SmartConfigPinOut;
				}
			}
			else
			{
				Config8266LedDelay=0;
			}
		
		2,调用使用,建议使用一个按钮控制
			
			if(SmartConfig())//配网成功
			{
				//执行操作
			}
  ******************************************************************************
	APUConfig配网绑定流程
	设备端
	1.获取设备MAC XX:XX:XX:XX:XX:XX
	2.控制WIFI发出固定无线网  名称:wifi_8266_bind    密码:11223344  
	3.UDP 监听固定端口5556
	4.等待接收客户端的消息. 消息格式{"ssid":"qqqqq","pwd":"11223344"} 
	5.提取路由器名称和密码,连接路由器
	6.获取链接路由器后分的的IP. 假设是192.168.10.2 以防后期实现局域网通信备用 
	7.UDP发送数据,{"mac":"XX:XX:XX:XX:XX:XX","ip":"192.168.10.2"}
	
	APP/小程序/上位机
	1.提示用户连接自己的路由器,长按设备按钮使得设备进入UDP监听状态,提示用户输入路由器密码
	2.用户点击绑定设备 , 控制手机尝试连接 名称为 wifi_8266_bind 的无线 (内部控制)
	3.成功连接无线,往192.168.4.1:5556 UDP发送路由器信息,1S 1次
	4.接收到 {"mac":"XX:XX:XX:XX:XX:XX","ip":"192.168.10.2"}
	5.绑定成功
	8.完
  */

#define CONFIG8266_C_
#include "include.h"

char SmartConfigFlage = 0;//是不是在配网
u32  SmartConfigCnt = 0;//配网连接路由器延时使用
char SmartConfigSuccess = 0;//是否配网成功
u32  Config8266Delay=0;//执行Config8266函数内部所需延时
u32  Config8266LedDelay=0;//配置8266指示灯闪耀


char ThisSSID[32]="";//记录路由器名称
char ThisPWD[64]="";//记录密码
char ThisMAC[18]="";//记录设备MAC
char ThisIP[21]="";//记录设备连接路由器分得的IP


/**
* @brief  启用APUConfig 给WIFI配网
* @ warn  None
* @param  None
* @param  None
* @param  None
* @param  None
* @retval 1:成功
* @example 
**/
char APUConfig(void)
{
  u32 delay=0,Flage=0;
	SmartConfigPinOut = 1;
	SmartConfigSuccess = 0;
	Rst8266();
	
	if(ConfigModuleBlock("+++","+++",NULL))//退出透传
	{
		if(ConfigModuleBlock("AT+RESTORE\r\n","ready",NULL))//恢复出厂设置
		{
			if(ConfigModuleBlock("AT+CWMODE_DEF=3\r\n","OK",NULL))//模式3
			{
				if(ConfigModuleBlock("AT+CIPSTAMAC_CUR?\r\n","MAC_CUR",NULL))//MAC
				{
					MainString = StrBetwString(Usart1ReadBuff,"MAC_CUR:\"","\"");//得到MAC
	
					if(strlen(MainString) ==17)
					{
						memset(ThisMAC,0,sizeof(ThisMAC));
						memcpy(ThisMAC,MainString,17);
					}
					else {goto end;}
					cStringRestore();
					
					if(ConfigModuleBlock("AT+CWSAP_DEF=\"wifi_8266_bind\",\"11223344\",11,4,4\r\n","OK",NULL))//配置发出的无线
					{
						if(ConfigModuleBlock("AT+CIPSTART=\"UDP\",\"192.168.4.2\",5555,5556,2\r\n","OK",NULL))//配置UDP
						{
							SmartConfigCnt = 0;
							while(1)
							{
								//{"ssid":"qqqqq","pwd":"11223344"}
								//{"mac":"XX:XX:XX:XX:XX:XX","ip":"192.168.10.2"}
								//*StrBetwString(char *Str,char *StrBegin,char *StrEnd)
								IWDG_Feed();//喂狗
								if(Usart1ReadFlage==1)
								{
									Usart1ReadFlage=0;
									
									MainString = StrBetwString(Usart1ReadBuff,"\"ssid\":\"","\"");//获取ssid
									if(MainString!=NULL) 
									{
										memset(ThisSSID,0,sizeof(ThisSSID));
										sprintf(ThisSSID,"%s",MainString);
										cStringRestore();
										
										MainString = StrBetwString(Usart1ReadBuff,"\"pwd\":\"","\"");//获取pwd
										if(MainString!=NULL) 
										{
											memset(ThisPWD,0,sizeof(ThisPWD));
											sprintf(ThisPWD,"%s",MainString);
											cStringRestore();
											break;
										}
										else {goto end;}
									}
									else {goto end;}
								}
								
								if(SmartConfigCnt>60000) {goto end;}//60S超时
							}

							if(ConfigModuleBlock("AT+CWAUTOCONN=1\r\n","OK",NULL))//自动连接路由器
							{
								memset(MainBuffer,0,sizeof(MainBuffer));
								sprintf(MainBuffer,"AT+CWJAP_DEF=\"%s\",\"%s\"\r\n",ThisSSID,ThisPWD);
								if(ConfigModuleBlock(MainBuffer,"WIFI GOT IP",NULL))//设置连接的路由器
								{
									Flage = 1;//配网成功
									SmartConfigSuccess=1;
									
									if(ConfigModuleBlock("AT+CIPSTA_CUR?\r\n","CIPSTA_CUR:ip",NULL))//获取路由器分得的IP
									{
										MainString = StrBetwString(Usart1ReadBuff,"CUR:ip:\"","\"");//得到路由器分得的IP
										if(MainString != NULL)
										{
											memset(ThisIP,0,sizeof(ThisIP));
											memcpy(ThisIP,MainString,strlen(MainString));
											
											split(MainString,".",NULL,&MainLen);//XXX.XXX.XXX.XXX
											
											if(MainLen == 4)
											{
												MainLen = sprintf(MainBuffer,"{\"mac\":\"%s\",\"ip\":\"%s\"}",ThisMAC,ThisIP);
												MainLen = sprintf(MainBuffer,"AT+CIPSEND=%d\r\n",MainLen);

												if(ConfigModuleBlock(MainBuffer,">",NULL))//准备向UDP客户端发送消息
												{
													memset(MainBuffer,0,sizeof(MainBuffer));
													MainLen = sprintf(MainBuffer,"{\"mac\":\"%s\",\"ip\":\"%s\"}",ThisMAC,ThisIP);
													printf("%s",MainBuffer);
													
													SmartConfigCnt = 0;
													while(SmartConfigCnt<2000)
													{
														IWDG_Feed();//喂狗
													}
												}else {goto end;}
												
												MainLen = sprintf(MainBuffer,"{\"mac\":\"%s\",\"ip\":\"%s\"}",ThisMAC,ThisIP);
												MainLen = sprintf(MainBuffer,"AT+CIPSEND=%d\r\n",MainLen);

												if(ConfigModuleBlock(MainBuffer,">",NULL))//准备向UDP客户端发送消息
												{
													memset(MainBuffer,0,sizeof(MainBuffer));
													MainLen = sprintf(MainBuffer,"{\"mac\":\"%s\",\"ip\":\"%s\"}",ThisMAC,ThisIP);
													printf("%s",MainBuffer);
													
													SmartConfigCnt = 0;
													while(SmartConfigCnt<2000)
													{
														IWDG_Feed();//喂狗
													}
												}else {goto end;}
												
											}else {goto end;}
										}else {goto end;}
										cStringRestore();
									}
								}
							}
						}
					}
				}
			}
		}
	}
	end:
	if(ConfigModuleBlock("AT+CWMODE_DEF=1\r\n","OK",NULL))//模式1
	{}
	Rst8266();//复位
	SmartConfigFlage = 0;
	return Flage;
}



/**
* @brief  启用SmartConfig 给WIFI配网
* @ warn  None
* @param  None
* @param  None
* @param  None
* @param  None
* @retval 1:成功
* @example 
**/
char SmartConfig(void)
{
  u32 delay=0,Flage=0;
	SmartConfigPinOut = 1;
	SmartConfigSuccess = 0;
	Rst8266();
	
	if(ConfigModuleBlock("+++","+++",NULL))//退出透传
	{
		if(ConfigModuleBlock("AT+RESTORE\r\n","ready",NULL))//恢复出厂设置
		{
			if(ConfigModuleBlock("AT+CWMODE_DEF=1\r\n","OK",NULL))//模式1
			{
				if(ConfigModuleBlock("AT+CWAUTOCONN=1\r\n","OK",NULL))//自动连接路由器
				{
					if(ConfigModuleBlock("AT+CWSTARTSMART=3\r\n","OK",NULL))//配网(支持SmartConfig和微信AirKiss)
					{
						SmartConfigCnt = 0;
						while(1)
						{
							IWDG_Feed();//喂狗
							if(Usart1ReadFlage==1)
							{
								Usart1ReadFlage=0;
								if(strstr(Usart1ReadBuff,"WIFI CONNECTED") || strstr(Usart1ReadBuff,"WIFI GOT IP"))
								{
									SmartConfigPinOut = 1;//设备连接上路由器
									break;
								}
							}
							if(SmartConfigCnt>30000)
							{
								break;
							}
						}
						if(SmartConfigCnt>30000)//配网超时
						{
							SmartConfigPinOut = 0;
						}
						else
						{
							Flage = 1;
							SmartConfigSuccess=1;
							delay = SmartConfigCnt+5000;//延时5S
							while(SmartConfigCnt<delay)//让WIFI把MAC信息传给APP
							{
								IWDG_Feed();//喂狗
							}
						}
					}
				}
			}
		}
	}
	Rst8266();//复位
	SmartConfigFlage = 0;
	return Flage;
}



/**
* @brief  复位8266
* @ warn  单片机 Rst8266Pin 引脚需要和8266 RST引脚相连接
* @param  None
* @param  None
* @param  None
* @param  None
* @retval None
* @example 
**/
void Rst8266(void)
{
	Rst8266PinOut = 0;
	delay_ms(500);
	Rst8266PinOut = 1;
}


/**
* @brief  连接路由器
* @ warn  None
* @param  None
* @param  None
* @param  None
* @param  None
* @retval None
* @example 
**/
void Init8266(void)
{
	Rst8266();
	ConfigModuleBlock("+++","+++",NULL);//退出透传
	ConfigModuleBlock("AT+RESTORE\r\n","ready",NULL);//回复出厂设置
	ConfigModuleBlock("AT+CWMODE_DEF=1\r\n","OK",NULL);//模式1
	ConfigModuleBlock("AT+CWAUTOCONN=1\r\n","OK",NULL);//自动连接路由器
  ConfigModuleBlock("AT+CWJAP_DEF=\"qqqqq\",\"11223344\"\r\n","OK","WIFI CONNECTED");//设置连接的路由器
	Rst8266();//复位
}
